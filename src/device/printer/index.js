const Escpos = require('escpos');
Escpos.USB = require('escpos-usb');


let usbPrinters ;
let activePrinter;

module.exports = {

  findUSBPrinters: () => {
    return Escpos.USB.findPrinter();
  },

  findPrinter: () => {
    usbPrinters = Escpos.USB.findPrinter();
    if(usbPrinters.length) {
      activePrinter = usbPrinters[0];
    }
    return activePrinter;
  },

  print: ({encoding = 'GB18030', width = 48}) => new Promise((resolve) => {
    const device = new Escpos.USB(activePrinter.deviceDescriptor.idVendor, activePrinter.deviceDescriptor.idProduct);
    const printerInst = new Escpos.Printer(device, {
      encoding,
      width,
    });
    device.open(function (error) {
      resolve(printerInst);
    });
  }),

}