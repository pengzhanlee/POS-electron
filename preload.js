// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.

const Printer = require('./src/device/printer');
const {machineIdSync} = require('node-machine-id');

const App = {
  Printer,
  DEVICE_ID: machineIdSync(),
}

window.App = App;