// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron');
const path = require('path');
const {is} = require('electron-util');
const packageInfo = require('./package.json');

const IS_DEV = is.development;

function createWindow () {
  // Create the browser window.
  const splash = new BrowserWindow({
    center: true,
    width: 800,
    height: 450,
    frame: false,
    transparent: true,
    alwaysOnTop: true,
    show: true,
    // hasShadow: false,
    webPreferences: {
      nodeIntegration: true
    }
  });

  const mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    title: 'Veeyung POS',
    frame: false,
    show: false,
    backgroundColor: '#ccc',
    fullscreen: !IS_DEV,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  });

  const loadFinish = () => {
    mainWindow.show();
    splash.destroy();
  }

  const loadFail = (e) => {
    splash.webContents.send('load-fail', {code: e.code, errno: e.errno});
  }

  splash.loadURL(`file://${__dirname}/src/web/splash.html`).then(()=>{
    splash.webContents.send('version', packageInfo.version);

    // and load the index.html of the app.
    if(IS_DEV) {
      mainWindow.loadURL('http://localhost:3000/login?return=/POS/checkout').then(() => {
        loadFinish();
      }).catch(loadFail);
    }else {
      mainWindow.loadURL('https://admin.veeyung.com/login?return=/POS/checkout').then(() => {
        loadFinish();
      }).catch(loadFail);
    }

    // Open the DevTools.
    if(IS_DEV) {
      mainWindow.webContents.openDevTools();
    }
  });


  // mainWindow.once('ready-to-show', () => {
  //   console.log('ready');
  //   mainWindow.show();
  //   splash.destroy();
  // });

  return mainWindow;
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.allowRendererProcessReuse = false;
app.whenReady().then(() => {
  createWindow()
  
  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
